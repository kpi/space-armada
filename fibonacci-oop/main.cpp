#include <iostream>

using namespace std;

class Fibonacci {
public:
    static unsigned long long fibonacci(unsigned int n) {
        unsigned long long aff[100000];
        aff[0] = 1;
        aff[1] = 1;
        for (unsigned int i = 2; i < n; ++i)
            aff[i] = aff[i - 1] + aff[i - 2];
        return aff[n - 1];
    }
};

class Node {
public:
    unsigned long long value;
    Node *left;
    Node *right;

    // constructor // inline implementation for clarify
    explicit Node(unsigned long long a) {
        this->value = a;
        this->left = nullptr;
        this->right = nullptr;
    }

    // destructor
    ~Node() {
        // delete null pointer has no effect
        delete left;
        delete right;
    }

    // печаталка
    static void print(Node *node, unsigned int depth) {
        if (node == nullptr) return;
        else {
            cout << "node's value = " << node->value << endl;
            if (depth != 0) {
                print(node->left, depth - 1);
                print(node->right, depth - 2);
            } else {
                cout << endl;
            }
        }
    }

};

class Tree {
public:
    Node *root;
    explicit Tree(int depth) {
        if (depth >= 0) {
            if (depth == 0) {
                root = new Node(0);
            } else if (depth == 1) {
                root = new Node( 0);
                root->left = new Node(root->value - Fibonacci::fibonacci(depth));
                create_fibonacci_tree(root->left, depth - 1);
            } else {
                root = new Node( 0);
                root->left = new Node(root->value - Fibonacci::fibonacci(depth));
                root->right = new Node(root->value + Fibonacci::fibonacci(depth));
                create_fibonacci_tree(root->left, depth - 1);
                create_fibonacci_tree(root->right, depth - 2);
            }
        }
    }

    ~Tree() {
        delete root;
    }

    static void change_tree_node(struct Node *node, unsigned long long value);

    static struct Node *create_fibonacci_tree(struct Node *root, int depth);

    static struct Node *fill_fibonacci_tree(struct Node *root, int depth, unsigned long long current_number);
};

void Tree::change_tree_node(struct Node *node, unsigned long long value) {
    node->value = value;
    node->left = nullptr;
    node->right = nullptr;
}

struct Node *Tree::create_fibonacci_tree(struct Node *root, int depth) {
    if (depth >= 0) {
        if (depth == 0)
            change_tree_node(root, 0);
        else if (depth == 1) {
            change_tree_node(root, 0);
            root->left = new Node(root->value - Fibonacci::fibonacci(depth));
            create_fibonacci_tree(root->left, depth - 1);
        } else {
            change_tree_node(root, 0);
            root->left = new Node(root->value - Fibonacci::fibonacci(depth));
            root->right = new Node(root->value + Fibonacci::fibonacci(depth));
            create_fibonacci_tree(root->left, depth - 1);
            create_fibonacci_tree(root->right, depth - 2);
        }
        return root;
    } else {
        return nullptr;
    }
}

struct Node *Tree::fill_fibonacci_tree(struct Node *root, int depth, unsigned long long current_number) {
    if (depth >= 0) {
        if (depth == 0)
            change_tree_node(root, current_number);
        else if (depth == 1) {
            change_tree_node(root, current_number);
            root->left = new Node(root->value - Fibonacci::fibonacci(depth));
            fill_fibonacci_tree(root->left, depth - 1, root->value - Fibonacci::fibonacci(depth));
        } else {
            change_tree_node(root, current_number);
            root->left = new Node(root->value - Fibonacci::fibonacci(depth));
            root->right = new Node(root->value + Fibonacci::fibonacci(depth));
            fill_fibonacci_tree(root->left, depth - 1, root->value - Fibonacci::fibonacci(depth));
            fill_fibonacci_tree(root->right, depth - 2, root->value + Fibonacci::fibonacci(depth));
        }
        return root;
    } else {
        return nullptr;
    }
}

int main() {
    int depth;
    cin >> depth;
    Tree *tree = new Tree(depth);
    Tree::fill_fibonacci_tree(tree->root, depth, Fibonacci::fibonacci(depth + 2));
    Node::print(tree->root, depth);
}