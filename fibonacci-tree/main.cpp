#include <iostream>

using namespace std;

struct Node {
    Node *left;
    Node *right;
    unsigned long value;

    // и так тоже
    Node(unsigned long value, Node *left) : value(value), left(left), right(nullptr) {}

    // ничё личного
    Node(unsigned long value) : value(value), left(nullptr), right(nullptr) {}

    // дефолтовый кудаж без ниго #29
    Node() : value(0), left(nullptr), right(nullptr) {}
};

Node *build_from_proto(Node *proto, unsigned long main);

void test(Node *node);

int main() {
    unsigned long h;
    cout << "h = ?";
    cin >> h;

    // на всяка мудреца довольно простоты
    if (h <= 0) return 0;

    // как оазалось высота мерится дерева это не высота узла + 1
    h++;

    // переменная голова дерева, НЕ инициализируем но используем в цикле и по выходу там будет ук. на голову созданную в цикле
    Node *node = new Node(0);

    for (unsigned long i = 1; i <= h; i++) {
        if (i == 1) {
            // дефолтовые вариации хардкодим
            node = new Node(1);
        } else if (i == 2) {
            // дефолтовые вариации хардкодим
            node = new Node(2, node);
        } else {
            // индукионная вариация основная, тоже потенциально можно сторить а не ходитьь за ними
            // надстраиваем голову
            unsigned long lv = node->value;
            unsigned long llv = node->left->value;
            unsigned long value = lv + llv;
            node = new Node(value, node);

            // достраивам право начиная с i>=3
            Node *proto = node->left->left;
            node->right = build_from_proto(proto, value);
        }
    }

    cout << node->value << endl;

    // test
    test(node);

}

void test(Node *node) {
    cout << node->value << ": ";
    if(node->left != nullptr && node->right != nullptr) {
        cout << node->left->value << " | " << node->right->value  << "=";
        cout << (node->value - node->left->value == node->right->value - node->value ? "ok" : "ERROR") << endl;
    }
    else if(node->left != nullptr && node->right == nullptr) {
        cout << node->left->value << " | " << "NULL" << "=";
        cout << (node->value - node->left->value ==1 ? "ok" : "ERROR") << endl;
    }
    else {
        cout << node->value << "ok" << endl;
    }


    if(node->left != nullptr)
        test(node->left );
    if(node->right != nullptr)
        test(node->right);
}

// отдельная функция достройки правой части
// начиная с 3 правая голова гарантирована
Node *build_from_proto(Node *proto, unsigned long main) {
    // центральный обход поддерева
    Node* new_node = new Node(proto->value+main);
    // лево
    if(proto->left != nullptr) {
        new_node->left = build_from_proto(proto->left, main);
    }
    // право
    if(proto->right != nullptr) {
        new_node->right = build_from_proto(proto->right, main);
    }

    return new_node;
}


//new_node = new Node();
//new_node->value = 2;
//new_node->left= node;
//new_node->right = NULL;